Conreal OCR

## Project folder structure:

<br>
In this project it contains the following files:<br>
<b>ocr_pdf</b> and <b>ocr_software</b> These are the folders for the Django project.<br>
<b>ocr_files</b> It contains the temporary files which is saved in that folder during model predictions.
<br><b>ocr_ner_weights</b> It contains the weight files which is generated when NER model is trained.

