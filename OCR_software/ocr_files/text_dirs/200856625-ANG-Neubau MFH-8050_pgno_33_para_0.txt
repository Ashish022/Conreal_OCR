            21100    5                   —           5116 630.100.000                                1'058.00           5'290.00 
            K        ST                              Spiegelschrank Schneider                          PG 44 
                                         |           Lowline Basic LED 4000 Kelvin 
                                   Bi    |           Breite 120 cm 
                                                     Höhe 70/74,5 cm, Tiefe 
                                                     12 cm, 2 Doppelspiegeltüre 
                                                     Doppelsteckdose oben rechts 
                                                     wechselbar nach links 
                                                     Beleuchtung 1 x 25 W, IP 44 
                                                     Aluminiumprofil 
                                                     Energieeffizienz A++ 
                                                     weiss 
                                                     Bitte beachten Sie den technischen Installationshinweis Nr. 1 in der Beilage. 