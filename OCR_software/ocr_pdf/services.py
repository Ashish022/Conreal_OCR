from pdf2image import pdfinfo_from_path,convert_from_path
import cv2
import pytesseract
from pytesseract import Output
import pandas as pd
import os
import re
import glob
import numpy as np
from numpy.lib.twodim_base import tri
from .constants import *
def pdf_to_images(pdf_file_path):
    info = pdfinfo_from_path(pdf_file_path, userpw=None, poppler_path=None)
    maxPages = info["Pages"]
    for page in range(1, maxPages+1, 10) : 
        convert_from_path(pdf_file_path, dpi=200, first_page=page, last_page = min(page+10-1,maxPages),output_folder=pdf_images_dir)

## Functions used
def valid_prod_id(address):
  try:
      if "_" in address:
        address=address.replace("_","")
      host_bytes = address.split('.')
      host_bytes = address.split('.')
      valid = [int(b) for b in host_bytes]
      if len(host_bytes) == 3:
        return True
  except:
      return False

def digits(num):
  if num.isdigit()==True:
    if len(num)==3 or 4 or 6:
      return True

def is_int(element):
    try:
        int(element)
        return True
    except ValueError:
        return False
def is_float_cols(element):
    try:
        float(element)
        return True
    except ValueError:
        return False
def is_float(element):
    if "'" in element:
        return True
    try:
        float(element)
        return True
    except ValueError:
        return False
def extract_product_entries(text):
  temp_var=''
  if '\n' in text:
    temp_var=text.split("\n",1)[1] 
  return temp_var

def flagging_values(df):
    for index,row in df.iterrows():
        for col in list(set(df.columns)-set(['address','object'])):
            if str(df.loc[index,col])=='nan':
                print("articlenuber    ",df.loc[index,'article_number'])
                df.loc[index,'flag']=1
    return df
def tesseract_text_extraction(page_no,i):
    print("page_no",page_no)
    text=''
    image = cv2.imread(os.path.join(pdf_images_dir,i))
    gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    custom_config = r'--psm 6'
    details = pytesseract.image_to_data(gray_image, output_type=Output.DICT, config=custom_config, lang='deu')
    total_boxes = len(details['text'])
    for sequence_number in range(total_boxes):
        if int(details['conf'][sequence_number]) >60:
            (x, y, w, h) = (details['left'][sequence_number], details['top'][sequence_number], details['width'][sequence_number],  details['height'][sequence_number])
            threshold_img = cv2.rectangle(gray_image, (x, y), (x + w, y + h), (0, 255, 0), 2)
    df = pd.DataFrame(details)
    # clean up blanks
    df1 = df[(df.conf!='-1')&(df.text!=' ')&(df.text!='')]
    # sort blocks vertically
    sorted_blocks = df1.groupby('block_num').first().sort_values('top').index.tolist()
    for block in sorted_blocks:
        curr = df1[df1['block_num']==block]
        sel = curr[curr.text.str.len()>3]
        char_w = (sel.width/sel.text.str.len()).mean()
        prev_par, prev_line, prev_left = 0, 0, 0
        text = ''
        for ix, ln in curr.iterrows():
            # add new line when necessary
            if prev_par != ln['par_num']:
                text += '\n'
                prev_par = ln['par_num']
                prev_line = ln['line_num']
                prev_left = 0
            elif prev_line != ln['line_num']:
                text += '\n'
                prev_line = ln['line_num']
                prev_left = 0

            added = 0  # num of spaces that should be added
            if ln['left']/char_w > prev_left + 1:
                added = int((ln['left'])/char_w) - prev_left
                text += ' ' * added 
            text += ln['text'] + ' '
            prev_left += len(ln['text']) + added + 1
        text += '\n'
    first_page=i.split("-")[-1].split(".")[0]    
    if int(first_page)==int('1'):
        print("file",i)
        cv2.imwrite(address_and_object_image,image)
        with open(address_and_object_file, 'w') as fout: fout.write(text)
    if int(first_page)==int('2'):
        print("file",i)
        cv2.imwrite(address_and_object_image1,image)
        with open(address_and_object_file1, 'w') as fout: fout.write(text)
    if int(first_page)==int('3'):
        print("file",i)
        cv2.imwrite(address_and_object_image2,image)
        with open(address_and_object_file2, 'w') as fout: fout.write(text)
    if int(first_page)==int('4'):
        print("file",i)
        cv2.imwrite(address_and_object_image3,image)
        with open(address_and_object_file3, 'w') as fout: fout.write(text)
    return text

def imp_lines_extraction(text1):
    imp_lines=[]
    for line_no,line_content in enumerate(text1.splitlines()):
        digit_cnt=0
        prod_id=0
        spaces=0
        for k,line in enumerate(line_content.split(" ")):
            if line=="":
                spaces+=1
            if digits(line)==True:
                digit_cnt+=1
            if valid_prod_id(line)==True:
                prod_id+=1
        if prod_id==1 and digit_cnt>=2:
            imp_lines.append(line_no)
    return imp_lines


def saving_content_files(imp_lines,text1,page_no,name):
    text_lines=[]
    for index in range(len(imp_lines)):
        if index+1 ==len(imp_lines):
            text_lines.append(text1.splitlines()[imp_lines[index]:])
        else:
            text_lines.append(text1.splitlines()[imp_lines[index]:imp_lines[index+1]])
    for j in range(len(text_lines)):
        with open(text_dirs+"/"+name+"_pgno_"+str(page_no)+"_"+"para_"+str(j)+".txt", mode='wt', encoding='utf-8') as myfile:
            myfile.write('\n'.join(text_lines[j]))  
def extract_text_from_images(name):
    for page_no,i in enumerate(os.listdir(pdf_images_dir)):
        text1=tesseract_text_extraction(page_no,i)
        imp_lines=imp_lines_extraction(text1)
        saving_content_files(imp_lines,text1,page_no,name)

def fetch_address_and_object(address_and_object_file): 
    address=[]
    object_no=[]
    with open(address_and_object_file, "r") as file1:
        fileasList= file1.readlines()
            # print(fileasList)
            #list crearion
        read_add=[]
        first_list=''
        second_list=[]
        final_second_str=''
        for data1 in fileasList:
            read_add.append(data1)
        read_add_str=" ".join(read_add)
        if 'Objekt' in read_add_str and 'Offerte' in read_add_str:
            first_list=read_add_str.split("Objekt")[0]
            second_list_str=read_add_str.split("Objekt")[1]
            final_second_str=second_list_str.split("Offerte")[0]
    address.append(first_list.strip())
    object_no.append(final_second_str.strip()) 

    print("add_list",len(address))
    print("object_no",object_no)     
    return address,object_no

def fetch_address_and_object_updated(address_and_object_file): 
    address=[]
    object_no=[]
    image = cv2.imread(address_and_object_file)
    print(image.shape)
    address_roi = image[253:253+400, 60:60+550]
    object_roi = image[653:653+400, 60:60+550]

    address_text=pytesseract.image_to_string(address_roi,lang='deu',config='-c preserve_interword_spaces=1')
    object_text=pytesseract.image_to_string(object_roi,lang='deu',config='-c preserve_interword_spaces=1')

    print("address:--",address_text)
    print("object:--",object_text)
    if 'Objekt' in object_text:
        address.append(address_text)
        object_no.append(object_text)
        return address,object_no
    else:
        address=[]
        object=[]
        return address,object

    # cv2.imshow("Address", address_roi)


def clean_qty_tgt_price(amt,tgt_price):
  if "'" in amt:
    amt=amt.replace("'","")
  if "'" in tgt_price:
    tgt_price=tgt_price.replace("'","")
  qty=float(amt)/float(tgt_price)
  return qty    

def preprocess_df(df):
    # df=df[['article_number','product','cleaned_product','quantity','target_price','amount','object','address']]
    df['product']=df['cleaned_product']
    df=df[['article_number','product','quantity','target_price','amount','object','address']]
    df['target_price'] =[str(i).replace("'","")  if "'" in str(i) else str(i) for i in df['target_price']]
    df['amount'] =[str(i).replace("'","")  if "'" in str(i) else str(i) for i in df['amount']]
    # df=df[['article_number','product','quantity','target_price','amount','object','address']]
    df['target_price']=[str(i).split(" ")[0] if len(str(i))!=0 else i for i in df['target_price'] ]
    df['amount']=[i.split(" ")[0] if len(i)!=0 else i for i in df['amount'] ]
    df['quantity']=[i.strip().split(" ")[0] if len(i)!=0 else i for i in df['quantity'] ]
    df['product']=[re.sub(' +', ' ', i) for i in df['product'] ]
    df['article_number'] = df.article_number.replace('\s+', ' ', regex=True)
    df['address'] = df.address.replace('\n', ' ', regex=True)
    df['object'] = df.object.replace('\n', ' ', regex=True)
    df['address'] = df.address.replace('\x0c', '', regex=True)
    df['object'] = df.object.replace('\x0c', '', regex=True)    
    df['address'] = df['address'].str.strip()
    df['object'] = df['object'].str.strip()

    for index,row in df.iterrows():
        first_two=df.loc[index,'product'].split(" ")[0]
        if first_two=="ST":
            print(first_two)
            df.loc[index,'product']=df.loc[index,'product'].replace(first_two,"")
        res = None
        item_sub=df.loc[index,'product']
        print(item_sub)
        temp = re.search(r'[a-z]+[A-Z]', item_sub, re.I)
        if temp is not None:
            res = temp.start()
            df.loc[index,'product']=df.loc[index,'product'][res:]
    df['product'] = df['product'].str.strip()

    ## Making quantity, target_price and amount as float
    for index,row in df.iterrows():
        for col in check_floats:
            if is_float_cols(row[col])==False:
                df.loc[index,col]=''

    df=df.replace(r'', np.nan)
    for index,row in df.iterrows():
        if pd.isnull(row['quantity'])==True and pd.isnull(row['amount'])==False and pd.isnull(row['target_price'])==False:
            print("samples_seleccted")
            qty=clean_qty_tgt_price(df.loc[index,'amount'],df.loc[index,'target_price'])
            df.loc[index,'quantity']=int(qty)
            print("cleaning values")
    df = df.astype(str)    
    return df


def extract_via_python(text):
    amt=''
    tgt_price=''
    #  
    items=[]
    for line_no,i in enumerate(text.splitlines()):
        if line_no==0:
            for j in i.split(" "):
                if j !="":
                    items.append(j)
    if len(items)!=0:
        amt=items[-1]
        tgt_price=items[-2]
        return amt,tgt_price
def clean_product_item(temp_product):
  final_clean_prod=[]
  clean_prod=[]
  for i in temp_product:
    # print(i.split("\n"))
    spaces=0
    for index,k in enumerate(i.split(" ")):
    #   print(k.split("\n"))
      if k=='':
        spaces+=1
        if spaces >=5:
          break
      else:
        clean_prod.append(k)
  clean_prod=[i for i in clean_prod if i !="\n"]
  return " ".join(clean_prod)

def model_entities_extraction(text_dirs,nlp2):
    quantity_lst=[]
    article_number_lst=[]
    target_price_lst=[]
    product_lst=[]
    amount_lst=[]
    trial_prd=[]
    for txt_file in os.listdir(text_dirs):
        temp_quantity=[]
        temp_article_number=[]
        temp_target_price=[]
        temp_product=[]
        temp_amount=[]
        
        f = open(os.path.join(text_dirs,txt_file), "r")
        text=f.read()
        amt,tgt_price=extract_via_python(text)
        # print(text)
        doc = nlp2(text)  
        for i in doc.ents:
            if i.label_=="quantity":
                if is_int(i.text)==True:
                    temp_quantity.append(i.text)
            elif i.label_=="article_number":
                if len(temp_article_number)==0:
                    temp_article_number.append(i.text)
            elif i.label_=="target_price":
                if is_float(i.text)==True:

                    temp_target_price.append(i.text)
                else:
                    res=extract_product_entries(i.text)
                    temp_product.append(res)
            elif i.label_=="amount":
                temp_amount.append(i.text)
            elif i.label_=="product":
                temp_product.append(i.text)
        temp_product1=clean_product_item(temp_product)
        product_lst.append(" ".join(temp_product))
        quantity_lst.append(" ".join(temp_quantity))
        article_number_lst.append(" ".join(temp_article_number))
        amount_lst.append(" ".join(temp_amount))
        target_price_lst.append(" ".join(temp_target_price))
        trial_prd.append("".join(temp_product1))

    address,object_entry=fetch_address_and_object_updated(address_and_object_image)
    print("ADDRESS",address)
    # if len(address)==1 and address[0]=='':
    if len(address) == 0:
        address,object_entry=fetch_address_and_object_updated(address_and_object_image1)
    if len(address) == 0:
        address,object_entry=fetch_address_and_object_updated(address_and_object_image2)
    if len(address) == 0:
        address,object_entry=fetch_address_and_object_updated(address_and_object_image3)

    print(address)
    print(object_entry)
    # Creating dataframe
    df=pd.DataFrame()
    df['quantity']=quantity_lst
    df['article_number']=article_number_lst
    df['amount']=amount_lst
    df['target_price']=target_price_lst
    df['product']=product_lst
    df['cleaned_product']=trial_prd
    if df.empty==True:
        df.loc[0,'object']=object_entry
        df.loc[0,'address']=address

    for index,row in df.iterrows():
        temp_obj=" ".join(object_entry)
        if "Objekt" in temp_obj:
            temp_obj=temp_obj.split("Objekt")[1]
            
        df.loc[index,'object']=temp_obj
        df.loc[index,'address']=" ".join(address)    
    # df.to_csv("extra.csv",index=False)

    return df

def create_dirs():
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError as err:
        print(err)
    for root, dirs, files in os.walk(directory, topdown=False):
        for name in files:
            os.remove(os.path.join(root, name))
        for name in dirs:
            os.rmdir(os.path.join(root, name))

    try:
        if not os.path.exists(pdf_images_dir):
            os.makedirs(pdf_images_dir)
    except OSError as err:
        print(err)

    try:
        if not os.path.exists(text_dirs):
            os.makedirs(text_dirs)
    except OSError as err:
        print(err)

    files = glob.glob(pdf_images_dir+"/*")
    for f in files:
        os.remove(f)

    files = glob.glob(text_dirs+"/*")
    for f in files:
        os.remove(f)

def is_file_valid(file_name):
    extention=file_name.name.lower().split('.')[-1]
    if extention =="pdf":
        return True
    else:
        return False

def extract_text_from_ad_ob_img(img,address_and_object_file):
    try:
        image = cv2.imread(img)
        gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        text=pytesseract.image_to_string(gray_image,lang='deu',config='-c preserve_interword_spaces=1')
        text = text.replace(r"\n", " ")
    except Exception as e:
        text=''
    print(text)
    with open(address_and_object_file, 'w') as fout: fout.write(text)
def validation_df(df):
    for index,row in df.iterrows():
        df.loc[index,'flag']=0
        if np.isnan(float(row['quantity']))==False and np.isnan(float(row['amount']))==False and np.isnan(float(row['target_price']))==False:
            if float(df.loc[index,'amount'])==float(df.loc[index,'target_price'])*float(df.loc[index,'quantity']):
                pass
            else:
                if float(df.loc[index,'amount'])==round(float(df.loc[index,'target_price'])*float(df.loc[index,'quantity']),2):
                    pass
                else:
                    df.loc[index,'flag']=1
        elif np.isnan(float(row['quantity']))==True or np.isnan(float(row['amount']))==True or np.isnan(float(row['target_price']))==True:
            df.loc[index,'flag']=1

    return df

def callback_api(df):
    df.to_json('temp.json', orient='records')
    import json
    
    f = open('temp.json')
    
    # returns JSON object as
    # a dictionary
    data = json.load(f)
    final_dict={}
    final_dict['records']=data
    final_dict['totalRecords']=len(data)
    for index,rec in enumerate(final_dict['records']):
        for k,l in rec.items():
            if k =="product":
                final_dict['records'][index][k]=final_dict['records'][index][k].replace("\n","")
                final_dict['records'][index][k]=final_dict['records'][index][k].replace('"',"")
                final_dict['records'][index][k]=final_dict['records'][index][k].replace("'","")
    k=json.dumps(final_dict)
    with open('temp.json', 'w') as f:
        json.dump(k, f)
    f = open('temp.json')
    data = json.load(f)
    return data