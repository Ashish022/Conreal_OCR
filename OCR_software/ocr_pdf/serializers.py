import os
from datetime import datetime
from django.conf import settings
from rest_framework import serializers
from pdf2image import convert_from_path, convert_from_bytes
from django.core.files.storage import FileSystemStorage

OCR_ROOT_FILE_DIR = settings.BASE_DIR


# Document Upload Serializer
class DocumentUploadSeralizer(serializers.Serializer):
    file = serializers.FileField(write_only=True)
    # file = serializers.ListField(child=serializers.FileField(write_only=True))
    # file_type = serializers.CharField(max_length=25, write_only=True)
    file_name = serializers.CharField(max_length=255, read_only=True)

    def validate(self, data):
        file = data.get('file', None)
        original_file_name = file.name
        filename = file.name
        print(filename)
        # time = datetime.utcnow()
        # file_name = str(time) + '_' + (filename.replace('(', '')).replace(')', '')

        # if not os.path.isdir(os.path.join(OCR_ROOT_FILE_DIR, 'media', 'docs')):
        #     os.makedirs(os.path.join(OCR_ROOT_FILE_DIR, 'media', 'docs'))
        # fs = FileSystemStorage(location=os.path.join(OCR_ROOT_FILE_DIR, 'media', 'docs'))
        # fs.save(file_name, file)
        # file_url = os.path.join(OCR_ROOT_FILE_DIR, 'media', 'docs', file_name)

        # if original_file_name.split('.')[-1] == 'pdf':
        #     if not os.path.exists(os.path.join(OCR_ROOT_FILE_DIR, 'media', 'images', file_name.split('.pdf')[0])):
        #         os.makedirs(os.path.join(OCR_ROOT_FILE_DIR, 'media', 'images', file_name.split('.pdf')[0]))
        #     # images = convert_from_path(os.path.join(OCR_ROOT_FILE_DIR, file_url), 5000)
        #     images = convert_from_path(file_url, 500)
        #     image_list = ''

        #     for i, image in enumerate(images):  # convert pdf pages to images
        #         # image_filename = str(datetime.utcnow()) + '_' + (filename.replace('(', '')).replace(')', '')
        #         image_name = file_name.split('.pdf')[0] + "_" + str(i) + "." + "png"
        #         image_url = os.path.join(OCR_ROOT_FILE_DIR, 'media', 'images', file_name.split('.pdf')[0], image_name)
        #         image.save(os.path.join(OCR_ROOT_FILE_DIR, 'media', 'images', file_name.split('.pdf')[0], image_name))
        #         image_list += image_url + ','
        return filename

