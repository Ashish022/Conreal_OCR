from html import entities
from django.shortcuts import render
from .serializers import DocumentUploadSeralizer
from rest_framework.response import Response
from django.http import JsonResponse, response
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status, generics
import de_core_news_sm
from django.http import HttpResponse
from pdf2image import convert_from_path, convert_from_bytes
import cv2
import pytesseract
from pytesseract import Output
import pandas as pd
from numpy.lib.twodim_base import tri
import os
import spacy
from .constants import *
from .services import *
import warnings
warnings.filterwarnings("ignore")
# OCR_ROOT_FILE_DIR = settings.BASE_DIR
import glob
import os
from io import BytesIO
# import xlsxwriter


create_dirs()
print("directories created")
print(pytesseract.get_tesseract_version() )
nlp = de_core_news_sm.load()
print(nlp.pipe_names)
nlp2=spacy.load(NER_MODEL)
nlp3=spacy.load(NER_MODEL_NEW)

print("NER model loaded")

class DocumentUpload(generics.CreateAPIView):

    def post(self, request):
        try:
            create_dirs()

            file = request.data['file']
            if file=='':
                    return Response({'success': 'false', 'message': "missing required key: 'file'"}, status=status.HTTP_400_BAD_REQUEST)
            if is_file_valid(file) ==False:
                return Response({'success': 'False', 'message': 'File format not supported'})
            with open(directory+"/"+str(file).lower(), 'wb+') as f:
                for chunk in file.chunks():
                    f.write(chunk)
            name=file.name.rsplit(".",maxsplit=1)[0].split("/")[-1]
            print(name)
            pdf_file_path=os.path.join(directory,str(file.name).lower())
            pdf_to_images(pdf_file_path)
            extract_text_from_images(name)
            df=model_entities_extraction(text_dirs,nlp2)
            df=preprocess_df(df)
            df=validation_df(df)
            df=flagging_values(df)

            # Remove flag column
            # df=df[['article_number','product','quantity','target_price','amount','object','address']]
            # data=callback_api(df)
            # import requests
            # r = requests.post(url = API_END_POINT, data = data)
            # r.status_code=300
            # if r.status_code==200:
                # return Response({"success": True, "message": "Data uploaded sucessfully"})
            # else:
                # return Response({"success": True, "message": "Unable to process pdf"})

            # Saving file in excel format
            with BytesIO() as b:
                with pd.ExcelWriter(b) as writer:
                    df.to_excel(writer, sheet_name="DATA 1", index=False)    
                filename = "{}.xlsx".format(name)
                # imported from django.http
                res = HttpResponse(
                b.getvalue(), # Gives the Byte string of the Byte Buffer object
                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                )
                res['Content-Disposition'] = f'attachment; filename="'+str(name)+'.xlsx"'
                return res
        except Exception as e:
            print("exception",e)
            return JsonResponse({"success": True, "message": "Unable to process this pdf"})

class DocumentUploadNew(generics.CreateAPIView):

    def post(self, request):
        try:
            create_dirs()

            file = request.data['file']
            if file=='':
                    return Response({'success': 'false', 'message': "missing required key: 'file'"}, status=status.HTTP_400_BAD_REQUEST)
            if is_file_valid(file) ==False:
                return Response({'success': 'False', 'message': 'File format not supported'})
            with open(directory+"/"+str(file).lower(), 'wb+') as f:
                for chunk in file.chunks():
                    f.write(chunk)
            name=file.name.rsplit(".",maxsplit=1)[0].split("/")[-1]
            print(name)
            pdf_file_path=os.path.join(directory,str(file.name).lower())
            pdf_to_images(pdf_file_path)
            extract_text_from_images(name)
            df=model_entities_extraction(text_dirs,nlp3)
            df=preprocess_df(df)
            df=validation_df(df)
            df=flagging_values(df)
            # Remove flag column
            # df=df[['article_number','product','quantity','target_price','amount','object','address']]
            # data=callback_api(df)
            # import requests
            # r = requests.post(url = API_END_POINT, data = data)
            # r.status_code=300
            # if r.status_code==200:
                # return Response({"success": True, "message": "Data uploaded sucessfully"})
            # else:
                # return Response({"success": True, "message": "Unable to process pdf"})

            # Saving file in excel format
            with BytesIO() as b:
                with pd.ExcelWriter(b) as writer:
                    df.to_excel(writer, sheet_name="DATA 1", index=False)    
                filename = "{}.xlsx".format(name)
                # imported from django.http
                res = HttpResponse(
                b.getvalue(), # Gives the Byte string of the Byte Buffer object
                content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                )
                res['Content-Disposition'] = f'attachment; filename="'+str(name)+'.xlsx"'
                return res


            # # Download csv file
            # response = HttpResponse(content_type='text/csv')
            # # response['Content-Disposition'] = 'attachment; filename=filename.csv'
            # response['Content-Disposition'] = 'attachment; filename="'+str(name)+'.csv"'
            # # df.to_csv("a.csv",index=False)
            # df.to_csv(path_or_buf=response,index=False)
            # return response
        except Exception as e:
            print("exception",e)
            return JsonResponse({"success": True, "message": "Unable to process this pdf"})
                