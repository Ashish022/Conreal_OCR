# from django.conf.urls import url
# from django.urls import path
from django.conf.urls import url
from django.urls import path
from rest_framework import routers
from . import views

urlpatterns = [
    path('upload/', views.DocumentUpload.as_view(), name='upload'),
    path('upload_new/', views.DocumentUploadNew.as_view(), name='upload_new'),

    # path('mob_verification_status', views.mob_verification_status.as_view(), name='mob_verification_status'),

]